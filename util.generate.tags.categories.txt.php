<?php	
/*
*----------	
*		util.generate.tags.categories.txt.php
*----------
*		generate the file dftc.tags.cloud.AN.txt and the file
*		dftc.tags.cloud.AC.txt. Each line of the TXT files
*		contains the Category (Tag) shown, the number of tools,
*		and the related CodeCategory. These TXT files are taken as 
*		input by the utility util.generate.tags.cloud.html.py 
*		to generate, in turn, the files dftc.tags.cloud.AN.html and
*		dftc.tags.cloud.AC.html. These HTML files are used in the 
*		main page of the Catalogue to show the Categories of the 
*		tools in a tag cloud style.
*/	
	
	if (PHP_SAPI !== 'cli' || isset($_SERVER['HTTP_USER_AGENT'])) {
		 echo "Operation not permitted";
		 exit(1);
	}

	include "config/dftc.db.php";
	
	$processes = array("AN", "AC");
	foreach ($processes as $p) {
	
		try {
			$fTagsCloud = fopen(getcwd() . "/dftc.tags.cloud." . $p . ".txt", "w");
		}
		catch (Exception $e) {
		   	echo "Could not open file " . getcwd() . "/dftc.tags.cloud." . $p . ".txt \n\n";
		}

		fwrite($fTagsCloud, "#each line contains the Category (Tag) shown, the number of tools,");
		fwrite($fTagsCloud, "the related CodeCategory, \n");
		fwrite($fTagsCloud, "#the order is from lowest number of tools to the highest tools\n");

		try {
			$qryCategories	  = "SELECT Category, CodeCategory FROM tblCategories WHERE ";		
			$qryCategories	 .= "Process='" . $p . "' ORDER BY CodeCategory";
			//*debug* echo "$qryCategories \n\n";
			$rsCategories 	= $db_conn->query($qryCategories);
			$nCategories	= $rsCategories->rowCount();
			$aValues = array();
			for ($i=0; $i<$nCategories; $i++) {
			$rowCategory = $rsCategories->fetch();
				$qryTools = "SELECT count(IdTool) as TotTools FROM tblToolsCategories WHERE CodeCategory LIKE '";
				$qryTools .= $rowCategory["CodeCategory"] . "%' AND Process='" . $p . "'";
				//*debug* echo $qryTools . "\n\n";
				$rsTools = $db_conn->query($qryTools);
				$rowTool = $rsTools->fetch();
				$nTot = $rowTool["TotTools"];
				$k = $rowCategory["Category"] . "|" . $rowCategory["CodeCategory"];
				$aValues[$k] = $nTot;
			}
			asort($aValues);
			foreach ($aValues as $key => $value) {
				list($category, $code) = explode("|", $key);
				fwrite($fTagsCloud, $category . "|" . $value . "|" . $code . "\n");
			}
			fclose($fTagsCloud);

		}
		catch (PDOException $e) {
			echo "ERROR in Insert FeaturesValues \n\n";
		   	echo $e->getMessage();
		   	exit;
		}
		echo "\nTags cloud  ". getcwd() . "/dftc.tags.cloud." . $p . ".txt generated!\n\n";
	}		

?>
