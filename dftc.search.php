<html>
<head>
<title>EVIDENCE project: Search tools frame</title>
<link rel="stylesheet" href="scripts/reset.css" type="text/css" />
<link rel="stylesheet" href="scripts/base.css" type="text/css" />
<link rel="stylesheet" href="scripts/jquery-ui.css" type="text/css" />
<script type="text/javascript" src="scripts/jquery-1.11.1.js"></script>
<script type="text/javascript" src="scripts/jquery-ui.js"></script>
<script type="text/javascript" src="scripts/dftc.search.js"></script>
<link rel="stylesheet" href="scripts/dftc.css" type="text/css"> 
</head>
<body onLoad=javascript:SetData("Analysis");>
<!--a><img src='images/dftc.evidence.logo.arrow.png' >
<img src='images/dftc.evidence.logo.acronym.png' alt='EVIDENCE Project' border='0'></a -->
<form name="frmSearch" method="post" action="dftc.results.php" target=content>
<p class=dftPulsanti align='center'>Forensics Tools Catalogue (<a class=dftLink href=javascript:Help(); title='Help on Catalogue'> Help? </a>)<br/>
<input type=hidden name=dfTools class=dftEnfasi2 size=10 value='Analysis'><br/></br-->
</p>
<!--p class=dftTextGrassetto>EVIDENCE project</p-->
<!--p><a href=# onClick=removePanel();>Remove FeaturePanel</a></p-->
<!--p><a href=# onClick=addPanel();>Add FeaturePanel</a></p-->
<table border='0'>
<?php    
   include "config/dftc.db.php";
   $qryTools      = 'SELECT DISTINCT tblTools.IdTool, Tool, LicenseType, OperatingSystem, tblToolsCategories.Process, Developer, Url, TestReport, Category, tblCategories.CodeCategory ';
   $qryTools     .= ' FROM tblTools, tblCategories, tblToolsCategories WHERE tblTools.IdTool=tblToolsCategories.IdTool AND tblCategories.CodeCategory=tblToolsCategories.CodeCategory AND ';
   $qryTools     .= 'tblToolsCategories.Process="AN" AND tblCategories.Process = "AN" ';    // Analysis
   $rsTools	     = $db_conn->query($qryTools);
   $nToolsAN = $rsTools->rowCount();
   
   $qryTools      = 'SELECT DISTINCT tblTools.IdTool, Tool, LicenseType, OperatingSystem, tblToolsCategories.Process, Developer, Url, TestReport, Category, tblCategories.CodeCategory ';
   $qryTools     .= ' FROM tblTools, tblCategories, tblToolsCategories WHERE tblTools.IdTool=tblToolsCategories.IdTool AND tblCategories.CodeCategory=tblToolsCategories.CodeCategory AND ';
   $qryTools     .= 'tblToolsCategories.Process="AC" AND tblCategories.Process = "AC" ';    // Acquisition
   $rsTools	     = $db_conn->query($qryTools);
   $nToolsAC = $rsTools->rowCount();
 
  echo "<tr class=dftText><td class=dftEnfasi5 >Total tools: <input class=dftEnfasi2 type=text name=totalTools readonly size=5 value=" . $nToolsAN . "></td></tr>"; 
  echo "<tr><td>&nbsp;</td></tr>";
  echo '<tr class=dftText align=center><td>';
  echo '<div id="radio">';
  echo '<input type="radio" id="rbAnalysis" name="rbProcess" value="AN" checked  onClick=javascript:SetData("Analysis");><label for="rbAnalysis">Analysis</label>';
  echo '<input type="radio" id="rbAcquisition" name="rbProcess" value="AC" onClick=javascript:SetData("Acquisition");><label for="rbAcquisition">Acquisition</label>';
  echo "<input type=hidden name=process value=AN>";
  echo '</div></td></tr>';
  echo "<tr><td>&nbsp;</td></tr>";
  echo "<tr class=dftText><td class=dftTextGrassetto>Name&nbsp;&nbsp;&nbsp;";
  echo "<input type=checkbox name=wholeCatalogue value=wholeCatalogue><span class=dftTextItalic>search over whole Catalogue</span></br>";
  echo "<input text name=toolName size=65 value='' onKeyPress=checkSubmit(event);></td></tr>";
  echo"<tr><td>&nbsp;</td></tr>";
  echo "<tr class='dftText'>";
  $qryLicense    = " SELECT DISTINCT LicenseType FROM tblTools";
  $rsLicense	     = $db_conn->query($qryLicense);
  $nLicense	 = $rsLicense->rowCount();
  echo "<td class=dftTextGrassetto>License type</br>
  <select name=license class=dftText>";
  echo "<option class='mainOption' value=''>All</option>";
  for($i=0; $i<$nLicense; $i++) {
    $rowLicense = $rsLicense->fetch();
    if (trim($rowLicense["LicenseType"] ) == "")
        ;
    else        
        echo "<option value='" . $rowLicense["LicenseType"] . "'>" . trim($rowLicense["LicenseType"]) . "</option>";		
  }
  echo "</select>";
  echo "</td>";
  echo "</tr>";

  echo"<tr><td>&nbsp;</td></tr>";
  echo "<tr class='dftText'><td class=dftTextGrassetto>Category <a href=javascript:MindMap(); border=0 title='Mind map'> (Mind Map, </a>";
  echo "&nbsp;<a href=javascript:TagsCloud(); title='Tags cloud on Category'>Tags cloud)</a></br>";
  $qryCategories    = "SELECT * FROM tblCategories WHERE Process='AN' ORDER BY CodeCategory";
  $rsCategoriesAN	     = $db_conn->query($qryCategories);
  $nCategoriesAN	 = $rsCategoriesAN->rowCount();
  $arrayCategoriesAN = $rsCategoriesAN->fetchAll();
  echo "<input name=Category class=dftHidden value=''> ";
  echo "<select name=CodeCategory class=dftText onChange=javascript:CheckCategory();>";
  echo "<option class='mainOption' value=''>All</option>";
  $aKeysFeatures = array();
  $aNamesFeatures = array();
  foreach($arrayCategoriesAN as $rowCategory) {
    $codeLen = strlen($rowCategory["CodeCategory"] );
    $space = str_repeat("&nbsp;",  $codeLen - 3);
    $sCategory = $rowCategory["Category"];
    $sCode = $rowCategory["CodeCategory"];
    //$middot = str_repeat("&middot;",  strlen($sCode));
    if ($codeLen == 3)    // main category
        //echo "<option class='mainOption' value='" . $rowCategory["CodeCategory"] . "'>" . $space .  $sCode . '&nbsp;' . $sCategory . "</option>";		
        echo "<option class='mainOption' value='" . $rowCategory["CodeCategory"] . "'>" . $space .  $sCode . '&nbsp;' . $sCategory . "</option>";		
    else
        echo "<option class='dftText' value='" . $rowCategory["CodeCategory"] . "'>" . $space . $sCode . '&nbsp;' .  $sCategory . "</option>";		
  }
    echo "</select>";
    
 // SELECT CodeCategoryAN, categories related to Analysis
  echo "<select name=CodeCategoryAN class=dftHidden>";
  echo "<option class='mainOption' value=''>All</option>";
  foreach($arrayCategoriesAN as $rowCategory) {
    $codeLen = strlen($rowCategory["CodeCategory"] );
    $space = str_repeat("&nbsp;",  $codeLen - 3);
    $sCategory = $rowCategory["Category"];
    $sCode = $rowCategory["CodeCategory"];
    if ($codeLen == 3)    // main category
        echo "<option value='" . $rowCategory["CodeCategory"] . "'>" . $space .  $sCode . '&nbsp;' . $sCategory . "</option>";		
    else
        echo "<option value='" . $rowCategory["CodeCategory"] . "'>" . $space . $sCode . '&nbsp;' .  $sCategory . "</option>";		
  }
    echo "</select>";

   // SELECT CodeCategoryAC, categories related to Acqusition
  $qryCategories    = "SELECT * FROM tblCategories WHERE Process='AC' ORDER BY CodeCategory";
  $rsCategoriesAC	     = $db_conn->query($qryCategories);
  $nCategoriesAC	 = $rsCategoriesAC->rowCount();    
  $arrayCategoriesAC = $rsCategoriesAC->fetchAll();
  echo "<select name=CodeCategoryAC class=dftHidden>";
  echo "<option class='mainOption' value=''>All</option>";
  foreach($arrayCategoriesAC as $rowCategory) {
    $codeLen = strlen($rowCategory["CodeCategory"] );
    $space = str_repeat("&nbsp;",  $codeLen - 3);
    $sCategory = $rowCategory["Category"];
    $sCode = $rowCategory["CodeCategory"];
    if ($codeLen == 3)    // main category
        echo "<option value='" . $rowCategory["CodeCategory"] . "'>" . $space .  $sCode . '&nbsp;' . $sCategory . "</option>";		
    else
        echo "<option value='" . $rowCategory["CodeCategory"] . "'>" . $space . $sCode . '&nbsp;' .  $sCategory . "</option>";		
  }
    echo "</select>";
  

// Features related to Categories of Analysis
//<select DeeperLevel may assume values S or N, if the related CodeCategory  is associated to Features with Sub Features
    echo "<select name=FeaturesAN class=dftHidden>";
    echo "<option value=''></option>";
    foreach($arrayCategoriesAN as $rowCategory){
        $qryFeatures = 'SELECT IdFeature, Feature, DeeperLevel, Visible FROM tblFeatures WHERE CodeCategory="' . $rowCategory["CodeCategory"]  . '" AND Process="AN" ORDER BY NumberFeature';
        $rsFeatures	     = $db_conn->query($qryFeatures);
        $nFeatures	 = $rsFeatures->rowCount();
        $keysFeatures = "";
        $namesFeatures = "";
        
        if ($nFeatures > 0) {
            for ($j=0; $j < $nFeatures; $j++) {
                $rowFeature = $rsFeatures->fetch();
                $keysFeatures .= $rowFeature[0] . "@" . $rowFeature[2] . "@" .  $rowFeature[3] .  "#" ;
                $namesFeatures .= $rowFeature[1] . "#" ;
            }
            $keysFeatures = substr($keysFeatures, 0, -1);
            $namesFeatures = substr($namesFeatures, 0, -1);    
            echo '<option value="'   .  $keysFeatures . '">' . $namesFeatures . '</option>';
            
        }           
        else 
            echo "<option value=''></option>";
    }            
     echo "</select>";
     
// Features related to Categories of Acquisition
//<select DeeperLevel may assume values S or N, if the related CodeCategory  is associated to Features with Sub Features
    echo "<select name=FeaturesAC class=dftHidden>";
    echo "<option value=''></option>";
    foreach($arrayCategoriesAC as $rowCategory) {
        $qryFeatures = 'SELECT IdFeature, Feature, DeeperLevel, Visible FROM tblFeatures WHERE CodeCategory="' . $rowCategory["CodeCategory"]  . '" AND Process="AC" ORDER BY NumberFeature';
        $rsFeatures	     = $db_conn->query($qryFeatures);
        $nFeatures	 = $rsFeatures->rowCount();
        $keysFeatures = "";
        $namesFeatures = "";
        
        if ($nFeatures > 0) {
            for ($j=0; $j < $nFeatures; $j++) {
                $rowFeature = $rsFeatures->fetch();
                $keysFeatures .= $rowFeature[0] . "@" . $rowFeature[2] . "@" .  $rowFeature[3] .  "#" ;
                $namesFeatures .= $rowFeature[1] . "#" ;
            }
            $keysFeatures = substr($keysFeatures, 0, -1);
            $namesFeatures = substr($namesFeatures, 0, -1);    
            echo '<option value="'   .  $keysFeatures . '">' . $namesFeatures . '</option>';
            
        }           
        else 
            echo "<option value=''></option>";
    }            
     echo "</select>";


     
    $qryValues = "SELECT IdFeature, Value FROM tblFeaturesValues ORDER BY IdFeature, Value";
    $rsValues	     = $db_conn->query($qryValues);
    $nValues	 = $rsValues->rowCount();
    
    $arrayValues = $rsValues->fetchAll();
    $rowValue = $arrayValues[0];
    $idOld = $rowValue["IdFeature"];
    $value = "";
    echo "<select name=FeaturesValues class=dftHidden>";
    foreach($arrayValues as $rowValue) {
        if ($idOld == $rowValue["IdFeature"])
            $value .=  trim($rowValue["Value"])  . "#";
        else {
            $value = substr($value, 0, -1);
            echo '<option value=' . $idOld . '>' . $value . '</option>';
            $idOld = $rowValue["IdFeature"];
            $value =  trim($rowValue["Value"]) . "#";
        }                        
    }
      $value = substr($value, 0, -1);
      echo '<option value=' . $idOld . '>' . $value . '</option>';
      echo "</select>";   
    
/*
// list SubFeatures names grouped by IdFeature; at the moment, 2014.12.20, only the 01.03.01 (Browser Forensics) has sub features.
    $qrySubFeatures = "SELECT IdSubFeature, IdFeature, SubFeature FROM tblSubFeatures ORDER BY IdFeature, NumberSubFeature";
    $rsSubFeatures     = $db_conn->query($qrySubFeatures);
    $nSubFeatures	 = $rsSubFeatures->rowCount();
    $rowSubFeature = $rsSubFeatures->fetch();
    $idOld = $rowSubFeature["IdFeature"];
    mysql_data_seek($rsSubFeatures, 0);
    $value = "";
    echo "<select name=SubFeatures class=dftHidden>";
    for  ($i=0; $i < $nValues; $i++) {
        $rowSubFeature = $rsSubFeatures->fetch();
        if ($idOld == $rowSubFeature["IdFeature"])
            $value .=  trim($rowSubFeature["SubFeature"])  . "@" . $rowSubFeature["IdSubFeature"] . "#";
        else {
            $value = substr($value, 0, -1);
            echo '<option value=' . $idOld . '>' . $value . '</option>';
            $idOld = $rowSubFeature["IdFeature"];
            $value =  trim($rowSubFeature["SubFeature"]) . "@" . $rowSubFeature["IdSubFeature"] ."#";
        }                        
    }
      $value = substr($value, 0, -1);
      echo '<option value=' . $idOld . '>' . $value . '</option>';
      echo "</select>"; 
*/
  echo "</td>";
  echo "</tr>";
  echo"<tr><td>&nbsp;</td></tr>";
  echo "<tr class='dftText'>";
  $qryOS    = " SELECT DISTINCT OperatingSystem FROM tblTools";
  $rsOSs	     = $db_conn->query($qryOS);
  $nOSs	 = $rsOSs->rowCount();
  echo "<td class=dftTextGrassetto>Operating System</br>
  <select name=os class=dftText>";
  echo "<option class='mainOption' value=''>All</option>";
  for($i=0; $i<$nOSs; $i++) {
    $rowOS = $rsOSs->fetch();
    if (trim($rowOS["OperatingSystem"] ) == "")
        ;
    else        
        echo "<option value='" . $rowOS["OperatingSystem"] . "'>" . trim($rowOS["OperatingSystem"]) . "</option>";		
  }
  echo "</select>";
  echo "</td>";
  echo "</tr>";
  echo "<tr><td>&nbsp;</td></tr>";
  echo "<tr class=dftText><td class=dftTextGrassetto>Developer/Reseller</br>";
  echo"<input text name=developer size=65></td></tr>"; 
  echo "<tr><td>&nbsp;</td></tr>";

  echo "<tr class=dftText><td class=dftTextGrassetto>Select  criteria and press  </span><input class='dftTextGrassetto'  type=button value='Search' OnClick=Go();></td></tr>";
  //echo "<tr class=dftText><td class=dftTextGrassetto>Url</br>";
  echo"<input type=hidden name=url size=65></td></tr>"; 


//  the dftc.boxsize.html must be regenerated every time a change occurs in a Category, Feature or Value, The new html file is 
//  created running the program util.generate.box.size.features.values.php using the command line on the Catalogue folder. 
  include("dftc.boxsize.php");
  

?>
</table>
<p>&nbsp;</p>
<div id="container"><p class='dftTextItalic' align='left'>Features Panel</p>
<div id="box">Box con testo</div>
</div>
<input type=hidden name=sort value='Tool'>
<input type=hidden name=direction value='ASC'>
<input type=hidden name=qryCatalogue value=''>

<?php
    echo '<input type=hidden name=nToolsAN value=' . $nToolsAN . '>';
    echo '<input type=hidden name=nToolsAC value=' . $nToolsAC . '>';
?>
</form>
</body>
</html>
