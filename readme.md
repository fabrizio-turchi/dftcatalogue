# Digital Forensic Tools Catalogue

## A Catalogue of Forensic Tools covering a wide range of Categories, from Mobile to Computer Forensic

### [Catalogue website](https://www.dftoolscatalogue.eu)


## The Need for a Digital Forensic Tools Catalogue

The increasing complexity of today’s digital forensics analyst activities depends on a multiple of elements that include, but are not limited to the following:
The increasing number of devices; the different operating systems; the huge number of applications, in particular devoted to mobile devices the different file formats to interpret and process.

The effectiveness of a forensic investigation depends mainly on the forensic analyst’s expertise and experience but also on the availability of forensic tools being able to extract and interpret data (potential evidence) quickly.

== Catalogue content
Currently the Digital Forensic Tools Catalogue (form now on Catalogue) comprises information on the most significant digital forensic tools related to:

1. Acquisition | 474 tools
2. Analysis | 1.084 tools

The total number of software tools considered so far is 1.558

## Configuration

The **dftc.config.php.sample** contains the constants to set up with their own DB data and then must be saved with the extension PHP

```php
/**
 *	DB connection configuration
 *
 * 	DB_HOST: DB hostname, usually it's "localhost", add Port if requested
 * 	DB_NAME: Catalogue DB name
 * 	DB_USER: Catalogue DB user
 * 	DB_PASS: Catalogue DB password
 */
define("DB_HOST", "localhost");
define("DB_NAME", "__DB-NAME__");
define("DB_USER", "__DB-USER__");
define("DB_PASS", "__DB-PASSWORD__");

// it is for managining the Tool Description when it is shown in the right frame
define("MAX_OFFSET", 10);
```
