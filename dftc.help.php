<?php
    include "config/dftc.db.php";
    $qryTools   = "SELECT count(IdTool) AS Tot from tblTools WHERE Process='AN'";
    $rsTools    = $db_conn->query($qryTools);
    $rowTool    = $rsTools->fetch();
    $nToolsAN = $rowTool["Tot"];

    $qryTools   = "SELECT count(IdTool) AS Tot from tblTools WHERE Process='AC'";
    $rsTools    = $db_conn->query($qryTools);
    $rowTool    = $rsTools->fetch();
    $nToolsAC = $rowTool["Tot"];

    $nTools = $nToolsAN + $nToolsAC;

?>

<html>
<head>
<title>EVIDENCE project: Results  frame</title>
<link rel="stylesheet" href="scripts/dftc.css" type="text/css"> 
</head>
<body class=dftText>
<a target="Evidence web site" href="http;//evidenceproject.eu">
<img div="logo" src="images/dftc.evidence.logo.png" alt="EVIDENCE project" border="0" />
</a>
<p class=dftPulsanti>Digital Forensic Tools - Catalogue</p>

<h2 class=dftEnfasi5>Catalogue structure</h2>
<p class=dftText>The catalogue comprises the most significant digital forensics tools related to:</p>

<ul>
<li><span class=dftTextGrassetto>Acquisition</span>: <?php echo $nToolsAC; ?> tools</li>
<li><span class=dftTextGrassetto>Analysis</span>: <?php echo $nToolsAN; ?> tools</li>
</ul>

<p class=dftText>

<?php
    $last = substr($nTools, -3);
    $total = substr($nTools, 0, -3) . "." . $last;
?>

The total number of software tools collected so far is <?php echo $total ?>.
</p>

<p class=dftText>The whole collection has been organized using a specific categorization briefly represented by the following hierarchical structure:</p>

<table border="0" width="100%">
<?php
    echo "<tr><td class=dftTextGrassetto colspan=2>A. Analysis</td></tr>";
    writeClassification("AN");
    echo "<tr><td class=dftTextGrassetto colspan=2>B. Acquisition</td></tr>";
    writeClassification("AC");
?>
</table>
        
<p class=dftText>Each tool is represented by the following data:</p>
<ol>
    <li> <span class=dftTextItalic>Tool Name</span>: it represents the name of the tool assigned to it by its producer/reseller/developer  
    <li> <span class=dftTextItalic>Tool Description</span>: it contains a short description of the tools, taken from its official web site  
    <li> <span class=dftTextItalic>License type</span>: it may assume values like Opensource, Freeware, Commercial, Multi (when it may have more than one single value)
    <li> <span class=dftTextItalic>Category</span>: it represents the hierarchical view of the different kind of forensics tools. Each tool may assume more than one single category: in this case the multiple occurrences of the same tool are separately considered and therefore are considered as two distinguished tools
    <li> <span class=dftTextItalic>Operating System</span>: it may assume values like: Windows, Mac, Linux, Standalone, Online, Hardware, Multi. Its value represents the operating 
    system on which the tool run.
    <li> <span class=dftTextItalic>Developer</span>: it is the author of the development of the tops and it may be a person, an organization or a community 
    <!--li> Web address: it is the official web site of the tool where it possible to read all the features and download documentation, technical card on the related tool<-->
    <li> <span class=dftTextItalic>Test report</span>: it is the official web address where a well known organization has tested the software and put the results of the operation on the web
    <li> <span class=dftTextItalic>Useful references</span>: it contains a list of web resources related to the tools, such as documentation, manual, unofficial tests and others
    <li> <span class=dftTextItalic>Features</span>: each Category is connected to a single or multiple features, even though, in some cases, it may not have any features at all. Each Feature may assume a single or multiple values.<br/>
    There are three cases:
    <ol type="a">
        <li><span class=dftTextGrassetto>Categories without Features</span>, for example: Computer Forensics (01.); File Analysis (02.); File Recovery / Carving (08.02), Forensics Toolkit (08.03)
        <li><span class=dftTextGrassetto>Categories with Single Feature</span>, for example:
        <ul>
            <li>Computer Forensics -> File System (01.01.), Feature=File System Supported
            <li> Computer Forensics -> Operating System -> Windows  System (01.02.01.), Feature=Scope
        </ul>                
        <li><span class=dftTextGrassetto>Category with Multiple Features</span>, for example: Browser Foreniscs (01.03.01.), Features=Internet Explorer, Firefox, Google Chrome, Safari, Opera
    </ol>
</ol>                    

<p class=dftText>Each Feature may assume multiple values, getting data from a default list of values</p>

<p class=dftText>Moreover there are tools that have been evaluated by NIST or other well known organizations in forensics field: when there are such tests they are shown below the related tool name  using a numbered icon. Sometimes the same tool may be evaluated by different organizations or from different perspectives, for example from the acquisition or analysis point of view. In these cases below the tool nam there will be as all the related links.
</p>

<h2 class=dftEnfasi5>Catalogue web pages</h2>
<p class=dftText>The Catalogue main page has been divided in two frames:

<ul>
    <li>the left frame or <span class=dftTextItalic>Search Frame</span>, is dedicated to query preparation
    <li>the right frame or <span class=dftTextItalic>Results Frame</span>,  shows the result of a query
</ul>

Operating on the <span class=dftTextItalic>Search Frame</span> the first step is selecting the <span class=dftTextItalic>Acquisition</span> or <span class=dftTextItalic>Analysis</span> branches, using the related radio button. As far as the user makes that choice the content of the search and results frames coherently change. More specifically the following information will  be modified:

<ul>
    <li> the total number of available tools    
    <li> the hierarchical structure of the Category field
    <li> the link related to Mind Map: it represent the mind map of the chosen branch - Acquisiton or Analysis
    <li> the link related to Tag Clouds: it shows a tag cloud representation of the Categories, within the selected branch, based on the number of tool belonging to the related Category
</ul>
</p>

<p class=dftText>The other fields useful for preparing a query are:
<ul>
    <li> the <span class=dftTextItalic>Name</span> of the tool: when the focus is on this field the Enter key is enabled and pressing it will run the query
    <li> the <span class=dftTextItalic>License Type</span>: it contains values like Freeware, Opensource, Commercial
    <li> the <span class=dftTextItalic>Operating System</span>: it contains values like Windows, Mac OS, Linux
    <li> the <span class=dftTextItalic>Category</span>: it is one of the main search field: it selects the branch of a particular digital forensics tools family. When the user select a Category the Features panel will show the related Features with all the possible values.
    <li> the <span class=dftTextItalic>Developer/Reseller</span>
</ul>

<p class=dftText>
The results are shown in a dynamic way and generally each field, such as Category, License, Operating System, is a link that allow to prepare and run a new query based on the current criteria adding the value of the field on which it has been activated the link itself .
</p>
</html>

<?php
function writeClassification($process) {
    global $db_conn;

    $qryCategories   = "SELECT CodeCategory, Category, Goal from tblCategories WHERE Process='";
    $qryCategories  .= $process . "' ORDER BY CodeCategory";
    $rsCategories   = $db_conn->query($qryCategories);
    $nCategories    = $rsCategories->rowCount();
    for ($i=0; $i<$nCategories; $i++) {
        $rowCategory = $rsCategories->fetch();
        $indent = ((strlen($rowCategory["CodeCategory"]) / 3) - 1) * 5;
        echo "<tr><td width=20% class=dftTextItalic>" . str_repeat("&nbsp;", $indent) . "&bull; ";
        echo $rowCategory["CodeCategory"] . " " . $rowCategory["Category"] . "</td>";
        echo "<td width=80% class=dftText>" . $rowCategory["Goal"] . "<br/></td></tr>";
    }

}
?>