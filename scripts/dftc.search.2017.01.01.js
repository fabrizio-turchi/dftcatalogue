function checkSubmit(e) {

    if(e && e.keyCode == 13)
        document.frmSearch.submit();
}

function Help() {
    parent.content.window.location.href = "dftc.help.php";    
}

function MindMap() {
    fProcess                    = document.frmSearch.process;

    if (fProcess.value == "AN")         // case Analysis
        parent.content.window.location.href = "dftc.analysis.pdf";
    else
        parent.content.window.location.href = "dftc.acquisition.pdf";      
        
}

function TagsCloud() {
    fProcess                    = document.frmSearch.process;

    if (fProcess.value == "AN")         // case Analysis
        parent.content.window.location.href = "dftc.tags.cloud.AN.html";
    else
        parent.content.window.location.href = "dftc.tags.cloud.AC.html";              
}

function SetData(process) {

    fCodeCategory     = document.frmSearch.CodeCategory;
    fCodeCategoryAN = document.frmSearch.CodeCategoryAN;    // Categories related to Analysis
    fCodeCategoryAC = document.frmSearch.CodeCategoryAC;    // Categories related to Acquisition
    
    if (process == "Analysis") {
        document.frmSearch.process.value="AN";
        document.frmSearch.totalTools.value = document.frmSearch.nToolsAN.value;
        selectLength = fCodeCategoryAN.length;
        fSelect = fCodeCategoryAN;
        document.frmSearch.dfTools.value = "Analysis";
        }            
    else {
        document.frmSearch.process.value="AC";
        document.frmSearch.totalTools.value = document.frmSearch.nToolsAC.value;
        selectLength = fCodeCategoryAC.length;
        fSelect = fCodeCategoryAC;
        document.frmSearch.dfTools.value = "Acquisition";
    }        
    
    fCodeCategory.length = selectLength
    for (i=0; i < selectLength; i++) {
        fCodeCategory.options[i].value = fSelect.options[i].value;
        fCodeCategory.options[i].text = fSelect.options[i].text;
    }    
    document.frmSearch.CodeCategory.selectedIndex = 0;
    document.frmSearch.license.selectedIndex = 0;
    document.frmSearch.toolName.value = "";
    document.frmSearch.os.selectedIndex = 0;
    document.frmSearch.developer.value = "";
    
    if (process == "Analysis") 
        parent.content.window.location.href="dftc.tags.cloud.AN.html";   
    else
        parent.content.window.location.href="dftc.tags.cloud.AC.html";   
                
    $("#box").hide("slow") 
}

function CheckCategory() {
    fProcess                    = document.frmSearch.process;
    fCategoryName         = document.frmSearch.Category;
    fCategories               = document.frmSearch.CodeCategory; 
    maxValuesFeature     = 1     // if a Feature has more then maxValuesFeature, then in the Features Panel are show two values for each row
    
    
    if (fProcess.value == "AN")         // case Analysis
        fFeatures                  = document.frmSearch.FeaturesAN;
    else                 // case Acquisition
        fFeatures                  = document.frmSearch.FeaturesAC;
    
    fFeaturesValues        = document.frmSearch.FeaturesValues;
    //fSubFeaturesValues  = document.frmSearch.SubFeatures;   


    idxCategory = fCategories.selectedIndex;
    vCategory = fCategories.options[idxCategory].value;
    idFeatures = fFeatures.options[idxCategory].value;
    nameFeatures = fFeatures.options[idxCategory].text;
    fCategoryName.value = fCategories.options[idxCategory].text;
    
    //window.alert("idx=" + idxCategory + "category=" + vCategory + ", \nidF=" + idFeatures + "\nnameF=" + nameFeatures);

    removePanel();
    
    if (idFeatures == "")  // if there isn't any feature  related to the chosen category
        ;
    else {   // there is, at least, a feature related to the chosen category, it shows the feature panel!
        divBox = $('<div id="box"></div>');
        divBox.appendTo("#container");
    }        
    
    aIdFeatures = idFeatures.split("#");                    // extract all features id separated by #
    aNamesFeatures = nameFeatures.split("#");      // extract all features names separated by #
    checkboxNumber = 0;
    for (k=0; k < aIdFeatures.length; k++) {            // loop on all Features related to the selected category
        idFeatureSubFeature = aIdFeatures[k].split("@");    // the value contains idFeature@DeeperLevel, DeeperLevel may assume S (with sub features) or N (without sub features)
        idFeature = idFeatureSubFeature[0];
        idSubFeature = idFeatureSubFeature[1];
        visibleFeature = idFeatureSubFeature[2]; 
        nameFeature = aNamesFeatures[k];
        valueFound = false;
        if (idSubFeature == "N") {                // feature without subfeatures
            for (i=0; i < fFeaturesValues.length; i++) {
                if (fFeaturesValues.options[i].value == idFeature) {
                    checkboxNumber ++;
//if the Feature is not visibile, case Category=03. and Feature=Function all values of that Feature are set on checked but the Feature is not shown on Features panel
                    if (visibleFeature =="N") {
                        classFeature  = " class=dftHidden ";
                        classRadio     = " class=dftHidden ";
                        sName = "";
                        checkboxNumber = 0;
                    }                        
                    else {
                        classFeature =" ";                    
                        classRadio = " class=dftTextGrassetto ";
                        sName = nameFeature
                    }                        
                           
                    if (checkboxNumber > 1)
                        labelRadio = "<hr><input type=checkbox " + classFeature + "  name=" + idFeature;
                    else
                        labelRadio = "<input type=checkbox " + classFeature + "  name=" + idFeature;    
                        
                    labelRadio = labelRadio + " onClick=javascript:SelectDeselectValues('" + idFeature + "');>";
                    labelFeature = $(labelRadio + '<span ' + classRadio + '> ' + sName + '</span><br/>');
                    labelFeature.appendTo("#box");
                    aValues = fFeaturesValues.options[i].text.split("#");    // extract all values separated by #
                    if (aValues.length > maxValuesFeature) { // if a Feature has more then maxValuesFeature, then in the Features Panel  two values for each row are shown
                        for (j=0; j < aValues.length; j++) {
                            nameCheckOne = idFeature + "_" + j;
                            valueCheckOne = aValues[j];
                            if (j == aValues.length - 1)
                                create_box_double_end(nameCheckOne, valueCheckOne, classRadio, sName); 
                            else {
                                idx = j + 1;
                                nameCheckTwo = idFeature + "_" + idx;
                                valueCheckTwo = aValues[idx];
                                create_box_double (nameCheckOne, valueCheckOne, nameCheckTwo, valueCheckTwo, classRadio, sName);
                                j = j + 1;
                            }                                                                
                        }
                    }
                    else {
                        for (j=0; j < aValues.length; j++) {
                            nameCheck = idFeature + "_" + j;
                            valueCheck = aValues[j];
                            create_box_special(nameCheck, valueCheck, classRadio, sName); 
                        }
                    }                        
                   valueFound = true;
                   break;
                }
            } 
            if (!valueFound){
                labelFeature = $('<span class=dftTextGrassetto><input type=checkbox name=' + idFeature + ' value="Yes"> ' + nameFeature + '</span><br/>');
                labelFeature.appendTo("#box");
            }
        }           
    }        
    
   idx = document.frmSearch.CodeCategory.selectedIndex;
   if (document.frmSearch.process.value == "AN")
        sizeBox = document.frmSearch.boxSizesAN.options[idx].value;
    else
        sizeBox = document.frmSearch.boxSizesAC.options[idx].value;               
        
    sizeContainer = (parseInt(sizeBox) + 30).toString();
    $("#container").css("height",sizeContainer);
    $("#box").css("height",sizeBox);
                    
   
    $("#box").toggle("slow");


 }
 
 function SelectDeselect(idFeature) {        // check box for selecting/deselecting all subfeature values
    var elementsLength = document.frmSearch.elements.length;
    //fSubFeatures = document.frmSearch.SubFeatures;
    
    lFound = false;
    for (i=0; i < elementsLength; i++) {
        if (frmSearch.elements[i].name == idFeature) {
            fFeature = frmSearch.elements[i];
            lFound = true;
            break;
        }            
    }
    if (lFound) {
       for (j=0; j<fSubFeatures.length; j++)  {
            if (fSubFeatures.options[j].value == fFeature.name) {
                //window.alert("before split #, " + fSubFeatures.options[j].text);
                valuesSubFeatures = fSubFeatures.options[j].text.split("#");
                for (k=0; k < valuesSubFeatures.length; k++) {
                    idSubF = valuesSubFeatures[k].split("@");
                    id = idSubF[1];
                    for (m=0; m < elementsLength; m++) {
                        if (frmSearch.elements[m].name == id) {
                            fSubFeature = frmSearch.elements[m];     
                            break;
                        }
                    }       
                    if (fFeature.checked)
                        fSubFeature.checked = true;
                    else
                        fSubFeature.checked = false;                                                
                }                    
            }                
        }           
    }        
}        

 function SelectDeselectValues(idFeature) {            // check box for selecting/deselecting all feature values
    var elementsLength = document.frmSearch.elements.length;
    var maxValuesFeature = 20;        // there isn't more than 20 values for each feature
    
    lFound = false;
    for (i=0; i < elementsLength; i++) {
        if (frmSearch.elements[i].name == idFeature) {
            fFeature = frmSearch.elements[i];
            lFound = true;
            break;
        }            
    }
    if (lFound) {
        value = fFeature.checked; 
        //window.alert(value); 
        for (j=0; j<maxValuesFeature; j++)  {
            valueFeature = idFeature + "_" + j;
            for (k=0; k < elementsLength; k++) {
                if (frmSearch.elements[k].name == valueFeature) {
                    fValue = frmSearch.elements[k];     
                    fValue.checked = value;
                    break;      
                } 
            }                                           
        }           
    }        
}        
  
  
function Go() {
  document.frmSearch.submit();
}

function alterna_box () {
	$("#box").toggle("slow");
//	      $("#box").hide("slow")
}

function create_box (nameCheck, valueCheck) {
	cb01 = $('<input type=checkbox   name=' + nameCheck + ' value="' + valueCheck + '">' + valueCheck + '<br/>')
    cb01.appendTo("#box");
}

function create_box_special (nameCheck, valueCheck, classRadio, sLabel) {
    if (sLabel == "")
        cb01 = $('<input type=checkbox ' + classRadio + '  name=' + nameCheck + ' value="' + valueCheck + '">')
    else
        cb01 = $('<input type=checkbox ' + classRadio + '  name=' + nameCheck + ' value="' + valueCheck + '">' + valueCheck + '<br/>')        
    cb01.appendTo("#box");
}

function create_box_double (nameCheck1, valueCheck1, nameCheck2, valueCheck2, classRadio, sLabel) {
    if (sLabel == "") {
        cbOne = '<tr><td><input type=checkbox ' + classRadio + '  name=' + nameCheck1 + ' value="' + valueCheck1 + '"></td>';
        cbTwo = '<td><input type=checkbox ' + classRadio + '  name=' + nameCheck2 + ' value="' + valueCheck2 + '"></td></tr>' ; 
    }        
    else {
        cbOne = '<tr><td><input type=checkbox ' + classRadio + '  name=' + nameCheck1 + ' value="' + valueCheck1 + '">' + valueCheck1 + '</td>';
        cbTwo = '<td><input type=checkbox ' + classRadio + '  name=' + nameCheck2 + ' value="' + valueCheck2 + '">' + valueCheck2 + '</td></tr>';
    }                
    
    cbDouble = $(cbOne + cbTwo)        
    cbDouble.appendTo("#box");
}

function create_box_double_end (nameCheck, valueCheck, classRadio, sLabel) {
    if (sLabel == "")
        cb01 = $('<tr><td colspan=2><input type=checkbox ' + classRadio + '  name=' + nameCheck + ' value="' + valueCheck + '"></td></tr>')
    else
        cb01 = $('<tr><td colspan=2><input type=checkbox ' + classRadio + '  name=' + nameCheck + ' value="' + valueCheck + '">' + valueCheck + '</td></tr>')  
    cb01.appendTo("#box");
}


function create_box_bold (nameCheck, valueCheck) {
	cb01 = $("<hr><strong><input type=checkbox    onClick=javascript:SelectDeselect('" + nameCheck + "'); name=" + nameCheck + ' value="' + valueCheck + '">' + valueCheck + '</strong><br/>')
    cb01.appendTo("#box");
}

function create_box_subfeatures (aName, aValue) {
    s = '<input type=checkbox     name=' + aName[0] + ' value="'  + aValue[0] + '">' + aValue[0];
    s += '<input type=checkbox   name=' + aName[1] + ' value="' + aValue[1] + '">' + aValue[1]; 
    s += '<input type=checkbox   name=' + aName[2] + ' value="' + aValue[2] + '">' + aValue[2] + '<br/>'; 
	cb01 = $(s)
    cb01.appendTo("#box"); 
}

function removePanel() {
    $("#box").remove() 
}
function addPanel() {
   divBox = $('<div id="box">Box con nuovo testo</div>');
    divBox.appendTo("#container");
   $("#box").toggle("slow");
        
}

$(function() {
    $( "#radio" ).buttonset();   
//    $(".dftTextRadio label").css("color","green");
//    $(".dftTextRadio label").css("background-image","none");
  });  